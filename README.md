# Story 8 | Library
Authored by:

Naufal Thirafy Prakoso

1806191723

PPW-D

# Pipeline and Coverage
[![pipeline status](https://gitlab.com/naufal.prakoso/story-7/badges/master/pipeline.svg)](https://gitlab.com/naufal.prakoso/story-8/commits/master)

[![coverage report](https://gitlab.com/naufal.prakoso/story-7/badges/master/coverage.svg)](https://gitlab.com/naufal.prakoso/story-8/commits/master)

# Herokuapp Link
copalibrary.herokuapp.com