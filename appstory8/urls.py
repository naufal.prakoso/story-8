from django.urls import path
from .views import index, data

app_name = 'appstory8'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='data'),
]